# 目录结构

![代码架构图](figures/construct.png)

一级目录为功能模块目录，比如wifi、ble、wifi_ble_combo等

二级目录为公司名称，比如swanlink等

三级目录为具体产品名，比如数字标牌digital_signage，最终路径为wifi/swanlink/digital_signage


# 使用介绍
[wifi 设备相关demo构建方法](./wifi/README_zh.md)
