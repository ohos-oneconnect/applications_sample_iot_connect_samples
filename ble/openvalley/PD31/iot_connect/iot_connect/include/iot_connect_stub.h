/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef IOT_CONNECT_STUB_H
#define IOT_CONNECT_STUB_H
#include "iremote_stub.h"
#include "message_parcel.h"
#include "parcel.h"
#include "i_iot_connect.h"

namespace OHOS {
namespace iotConnect {

class iot_connectStub : public IRemoteStub<Iiot_connectService> {
public:
    iot_connectStub();
    virtual ~iot_connectStub();
    int OnRemoteRequest(uint32_t code, MessageParcel& data, MessageParcel& reply,
        MessageOption &option) override;
private:
    using iot_connectInnerFunc = ErrCode (iot_connectStub::*)(MessageParcel &data, MessageParcel &reply);
    std::unordered_map<uint32_t, iot_connectInnerFunc> innerFuncs_;
};
}
}
#endif