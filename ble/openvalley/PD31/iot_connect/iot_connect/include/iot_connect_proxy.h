/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef IOT_CONNECT_PROXY_H
#define IOT_CONNECT_PROXY_H
#include "message_parcel.h"
#include "parcel.h"
#include "iremote_broker.h"
#include "iremote_proxy.h"
#include "i_iot_connect.h"

namespace OHOS {
namespace iotConnect {
class iot_connectProxy : public IRemoteProxy<Iiot_connectService> {
public:
    explicit iot_connectProxy(const sptr<IRemoteObject> &impl);
    ~iot_connectProxy() = default;
private:
    static inline BrokerDelegator<iot_connectProxy> delegator_;
};

class iot_connectDeathRecipient : public IRemoteObject::DeathRecipient {
public:
    virtual void OnRemoteDied(const wptr<IRemoteObject> &remote) override;
    iot_connectDeathRecipient();
    virtual ~iot_connectDeathRecipient();
};
}
}
#endif
