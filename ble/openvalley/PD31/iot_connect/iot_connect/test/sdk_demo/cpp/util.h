#ifndef IOTC_UTIL_H
#define IOTC_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus  
extern "C" {  
#endif  
  
bool GetScreenState();

#ifdef __cplusplus  
}  
#endif

#endif