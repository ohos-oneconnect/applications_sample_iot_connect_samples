#include <iostream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "power_mgr_client.h"

extern "C" {
    bool GetScreenState() {
        return OHOS::PowerMgr::PowerMgrClient::GetInstance().IsScreenOn();
    }
}