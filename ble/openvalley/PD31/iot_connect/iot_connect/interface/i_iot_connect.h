/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef I_IOT_CONNECT_H
#define I_IOT_CONNECT_H

#include <cstdint>
#include <vector>

#include "iremote_broker.h"
#include "iremote_proxy.h"

namespace OHOS {
namespace iotConnect {

class Iiot_connectService : public OHOS::IRemoteBroker {
public:
    enum {
        TEST,
    };
    DECLARE_INTERFACE_DESCRIPTOR(u"OHOS.iot_connect.Iiot_connect");
public:
};

}
}
#endif