/*
* Copyright (c) 2024 Hunan OpenValley Digital Industry Development Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef IOTCONNECT_LOG_H
#define IOTCONNECT_LOG_H

#include "hilog/log.h"

namespace OHOS {
namespace iotConnect {
static constexpr uint32_t IOTCONNECT_LOG_DOMAIN = 0xD002901;
static constexpr OHOS::HiviewDFX::HiLogLabel IOTCONNECT_LABEL = {
    LOG_CORE,
    IOTCONNECT_LOG_DOMAIN,
    "IOTCONNECT"
};

#ifndef IOTCONNECT_FUNC_FMT
#define IOTCONNECT_FUNC_FMT "[%{public}s@ %{public}s :%{public}d] "
#endif

#ifndef IOTCONNECT_FUNC_INFO
#define IOTCONNECT_FUNC_INFO __FUNCTION__, GetBriefFileName(__FILE__), __LINE__
#endif

static const __attribute__((unused)) char* GetBriefFileName(const char* name)
{
    static const char separator = '/';
    const char* p = strrchr(name, separator);
    return p != nullptr ? p + 1 : name;
}

#define HILOGD(fmt, ...) do { \
    OHOS::HiviewDFX::HiLog::Debug(IOTCONNECT_LABEL, IOTCONNECT_FUNC_FMT fmt, IOTCONNECT_FUNC_INFO, ##__VA_ARGS__); \
} while (0)

#define HILOG_ARRAY_UINT8(inArray, inArrayDataLen) (void)(inArray); (void)(inArrayDataLen);

#define HILOGI(fmt, ...) do { \
    OHOS::HiviewDFX::HiLog::Info(IOTCONNECT_LABEL, IOTCONNECT_FUNC_FMT fmt, IOTCONNECT_FUNC_INFO, ##__VA_ARGS__); \
} while (0)
#define HILOGW(fmt, ...) do { \
    OHOS::HiviewDFX::HiLog::Warn(IOTCONNECT_LABEL, IOTCONNECT_FUNC_FMT fmt, IOTCONNECT_FUNC_INFO, ##__VA_ARGS__); \
} while (0)
#define HILOGE(fmt, ...) do { \
    OHOS::HiviewDFX::HiLog::Error(IOTCONNECT_LABEL, IOTCONNECT_FUNC_FMT fmt, IOTCONNECT_FUNC_INFO, ##__VA_ARGS__); \
} while (0)
#define HILOGF(fmt, ...) do { \
    OHOS::HiviewDFX::HiLog::Fatal(IOTCONNECT_LABEL, IOTCONNECT_FUNC_FMT fmt, IOTCONNECT_FUNC_INFO, ##__VA_ARGS__); \
} while (0)

#ifndef IF_FALSE_RETURN_CODE
#define IF_FALSE_RETURN_CODE(cond, code, errDesc) \
    if (!(cond)) { \
        HILOGE("%{public}s", #errDesc); \
        return code; \
    }
#endif

#ifndef IF_TRUE_RETURN_CODE
#define IF_TRUE_RETURN_CODE(cond, code, errDesc) \
    if ((cond)) { \
        HILOGE("%{public}s", #errDesc); \
        return code; \
    }
#endif

#ifndef IF_NULLPTR_RETURN_CODE
#define IF_NULLPTR_RETURN_CODE(cond, code, errDesc) \
    if ((cond) == nullptr) { \
        HILOGE("%{public}s", #errDesc); \
        return code; \
    }
#endif
} // namespace IOTCONNECT
} // namespace OHOS
#endif // IOTCONNECT_LOG_H