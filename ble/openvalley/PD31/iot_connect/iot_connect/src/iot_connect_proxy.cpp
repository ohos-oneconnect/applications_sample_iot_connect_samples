#include "iot_connect_proxy.h"
#include "iot_connect_log.h"
#include <string.h>

using namespace std;

namespace OHOS {
namespace iotConnect {
iot_connectProxy::iot_connectProxy(const sptr<IRemoteObject> &impl) : IRemoteProxy<Iiot_connectService>(impl){}

void iot_connectDeathRecipient::OnRemoteDied(const wptr<IRemoteObject> &remote)
{

}

iot_connectDeathRecipient::iot_connectDeathRecipient()
{
}

iot_connectDeathRecipient::~iot_connectDeathRecipient()
{
}

}
}
