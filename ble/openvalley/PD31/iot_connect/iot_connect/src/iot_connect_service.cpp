#include <iostream>
#include <sstream>
#include <thread>
#include <map>
#include "iot_connect_service.h"
#include "system_ability_definition.h"
#include "iot_connect_log.h"
#include <vector>
#include <err.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "iservice_registry.h"
#include "securec.h"
#include "iotc_entry.h"
#include "power_mgr_client.h"
#include "bluetooth_host.h"
#include "bt_def.h"

using namespace std;
using namespace OHOS::Bluetooth;

#define ADV_TIMEOUT     UINT32_MAX

namespace OHOS {
namespace iotConnect {

REGISTER_SYSTEM_ABILITY_BY_ID(iot_connectService, IOT_CONNECT_ID, true)

class MyBluetoothObserver : public BluetoothHostObserver {
public:
    void OnStateChanged(const int transport, const int status) override {
        if (transport != ADAPTER_BLE) {
            HILOGE("Not a BLE transport, ignoring state change.");
            return;
        }
        switch (status) {
            case BTStateID::STATE_TURNING_ON:
                HILOGI("STATE_TURNING_ON");
                break;
            case BTStateID::STATE_TURN_ON:
                {
                    HILOGI("STATE_TURN_ON");
                    bool isScreenOn = PowerMgr::PowerMgrClient::GetInstance().IsScreenOn();
                    HILOGI("isScreenOn is %d", isScreenOn);
                    SetScreenState(isScreenOn);
                    HILOGI("before IotcOhReset");
                    {
                        int32_t ret = IotcOhReset();
                        // int32_t ret = IotcOhBleStartAdv(UINT32_MAX);
                        HILOGI("IotcOhReset ret is %{public}d", ret);
                    }

                    HILOGI("after IotcOhBleStartAdv");
                }
                break;
            case BTStateID::STATE_TURNING_OFF:
                HILOGI("STATE_TURNING_OFF");
                break;
            case BTStateID::STATE_TURN_OFF:
                HILOGI("STATE_TURN_OFF");
                break;
            default:
                HILOGI("UNKNOWN");
                break;
        }
    }

    // 实现基类中声明的所有纯虚函数
    void OnDiscoveryStateChanged(int status) override {
        (void)status;
    }

    void OnDiscoveryResult(const BluetoothRemoteDevice &device, int rssi, const std::string deviceName, int deviceClass) override {
        (void)device;
        (void)rssi;
        (void)deviceName;
        (void)deviceClass;
    }

    void OnPairRequested(const BluetoothRemoteDevice &device) override {
        (void)device;
    }

    void OnPairConfirmed(const BluetoothRemoteDevice &device, int reqType, int number) override {
        (void)device;
        (void)reqType;
        (void)number;
    }

    void OnScanModeChanged(int mode) override {
        (void)mode;
    }

    void OnDeviceNameChanged(const std::string &deviceName) override {
        (void)deviceName;
    }

    void OnDeviceAddrChanged(const std::string &address) override {
        (void)address;
    }
};

iot_connectService::iot_connectService(int32_t systemAbilityId, bool runOnCreate)
    :SystemAbility(systemAbilityId, runOnCreate)
{
    HILOGI("CaService in");
}

iot_connectService::~iot_connectService(){
    HILOGI("CaService out");
}

void iot_connectService::OnStart()
{
    HILOGI("iot_connectService start");
    auto myObserver = std::make_shared<MyBluetoothObserver>();//new
    Bluetooth::BluetoothHost::GetDefaultHost().RegisterObserver(myObserver);;//new
    HILOGI("RegisterObserver succeed");
    bool isPublished = Publish(this);
    if (!isPublished) {
        HILOGE("distributed_bioauthService Registration failed");
    }
    AddSystemAbilityListener(BLUETOOTH_HOST_SYS_ABILITY_ID);
    HILOGI("AddSystemAbilityListener end");
}

void iot_connectService::OnStop()
{
    HILOGI("iot_connectService stop");
}

void iot_connectService::OnAddSystemAbility(int32_t systemAbilityId, const std::string& networkID)
{
    HILOGI("OnAddSystemAbility start");
    sleep(5);
    HILOGI("OnAddSystemAbility systemAbilityId:%{public}d added!", systemAbilityId);
    bool isScreenOn = PowerMgr::PowerMgrClient::GetInstance().IsScreenOn();
    HILOGI("isScreenOn is %d",isScreenOn);
    SetScreenState(isScreenOn);
    HILOGI("before IotcOhDemoEntry");
    int32_t ret = IotcOhDemoEntry();
    HILOGI("IotcOhDemoEntry ret is %{public}d",ret);
    HILOGI("after IotcOhDemoEntry");
}

void iot_connectService::OnRemoveSystemAbility(int32_t systemAbilityId, const std::string& networkID)
{
    HILOGI("OnRemoveSystemAbility systemAbilityId:%{public}d removed!", systemAbilityId);
}

}
}
