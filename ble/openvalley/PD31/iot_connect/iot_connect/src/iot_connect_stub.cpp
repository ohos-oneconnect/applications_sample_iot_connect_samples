#include "iot_connect_stub.h"
#include "iot_connect_log.h"
#include <vector>
#include <string.h>
#include <unistd.h> 

using namespace std;

namespace OHOS {
namespace iotConnect {

iot_connectStub::iot_connectStub()
{
}

iot_connectStub::~iot_connectStub()
{
    innerFuncs_.clear();
}

int iot_connectStub::OnRemoteRequest(uint32_t code, MessageParcel& data, MessageParcel& reply,
    MessageOption &option)
{
    std::u16string descriptor = iot_connectStub::GetDescriptor();
    std::u16string remoteDescriptor = data.ReadInterfaceToken();
    if (descriptor != remoteDescriptor) {
        return OBJECT_NULL;
    }
    auto itFunc = innerFuncs_.find(code);
    if (itFunc != innerFuncs_.end()) {
        auto memberFunc = itFunc->second;
        if (memberFunc != nullptr) {
            return (this->*memberFunc)(data, reply);
        }
    }
    return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
}

}
}
