#include "iot_connect_proxy.h"
#include "ipc_skeleton.h"
#include "system_ability_definition.h"
#include "iservice_registry.h"
#include "iot_connect_client.h"
#include "iot_connect_log.h"

using namespace std;
using namespace OHOS;
using namespace OHOS::iotConnect;
namespace OHOS{

namespace iotConnect {
IMPLEMENT_SINGLE_INSTANCE(iot_connectClient);

sptr<Iiot_connectService> getRemoteProxy()
{
    auto saMgr = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
    if (saMgr == nullptr) {
        return nullptr;
    }
    sptr<IRemoteObject> object = saMgr->GetSystemAbility(IOT_CONNECT_ID);
    sptr<Iiot_connectService> proxy = nullptr;
    if (object != nullptr) {
        sptr<IRemoteObject::DeathRecipient> death(new iot_connectDeathRecipient());
        object->AddDeathRecipient(death.GetRefPtr());
        proxy = iface_cast<Iiot_connectService>(object);
    }
    if (proxy == nullptr) {
        HILOGE("proxy == nullptr");
        return nullptr;
    }
    return proxy;
}

}
}
