# BLE IOT_CONNECT


## 说明
### 使用说明
#### 代码路径
1. 将iot_connect目录放到foundation路径下，使得服务主路径为：foundation/iot_connect/iot_connect/src/iot_connect_service.cpp

2. 确保sdk存在于路径：foundation/communication/iot_connect下

3. 将selinux内iot_connect置于selinux目录下，路径为： base/security/selinux_adapter/sepolicy/ohos_policy/iot_connect
#### 服务配置
1. vendor/hihope/rk3568/config.json
```
/* 产品编译项配置 */ 
    {
      "subsystem": "iot_connect",
      "components": [
        {
          "component": "iot_connect",
          "features": []
        }
      ]
    }
```

2. build/subsystem_config.json
```
/* 编译项配置 */ 
  "iot_connect": {
    "path": "foundation/iot_connect/iot_connect",
    "name": "iot_connect"
  }
```

3. foundation/systemabilitymgr/samgr/interfaces/innerkits/samgr_proxy/include/system_ability_definition.h
```
/* 服务号配置 */ 
    IOT_CONNECT_ID                                   = 8101,
```

4. base/hiviewdfx/hidumper/frameworks/native/dump_utils.cpp
```
/* 服务名配置 */ 
    { IOT_CONNECT_ID, "IotConnect" },
```

5. developtools/syscap_codec/include/codec_config/syscap_define.h
```
/* 系统能力配置 */
/* 在前半部分配置 */
    IOT_CONNECT,
    ...
/* 在后半部分配置 */
    {"SystemCapability.Iot_Connect", IOT_CONNECT},
```

6. base/startup/init/services/etc/passwd

```
iot_connect:x:8101:8101:::/bin/false
```

7. base/startup/init/services/etc/group

```
iot_connect:x:8101:
```

#### sdk内部修改
1. SwitchPutCharState 亮灭屏幕效果

```
static int32_t SwitchPutCharState(const IotcServiceInfo *svc, const char *data, uint32_t len)
{
...
    g_switch = on == 0 ? false : true;
    if(on == 0){//new
        system("power-shell suspend");
    }else{//new
        system("power-shell wakeup");
    }
...
}
```

2. SetScreenState 屏幕设置接口

```
void SetScreenState(bool isScreenOn)//new
{
    DEMO_LOG("SwitchGetCharState in");
    DEMO_LOG("isScreenOn is %d",isScreenOn);
    g_switch = isScreenOn;
    DEMO_LOG("g_switch is %d",g_switch);
}
```
3. SetScreenState 屏幕获取接口

```
/* util.h */
extern "C" {  
#endif  
  
bool GetScreenState();

#ifdef __cplusplus  
} 

```

```
/* util.cpp */
extern "C" {
    bool GetScreenState() {
        return OHOS::PowerMgr::PowerMgrClient::GetInstance().IsScreenOn();
    }
}

```

4. 路径设置接口

```
/* iotc_oh_demo_ble.c */
SET_OH_SDK_OPTION(ret, IOTC_OH_OPTION_SDK_CONFIG_PATH, "/data/app/iotc");

```

在5.0及以上版本，需要关闭selinux
互联互通服务启动后，可以通过指令hidumper -ls 查看服务iot_connect
